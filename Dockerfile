# Dockerfile

FROM python:3.9-buster

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app
COPY ./opt/start-server.sh /usr/src/app
COPY ./webapp /usr/src/app
# COPY ./static /code/static
RUN python -m pip install -r requirements.txt

# start server
EXPOSE 8000
STOPSIGNAL SIGTERM
CMD ["/usr/src/app/start-server.sh"]
